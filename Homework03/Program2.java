import java.util.Scanner;

public class Program2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int leftDigit = 0;
        int rightDigit = 0;
        int minNumber = number;
        int minCount = 0;

        while (number != -1) {
            if (leftDigit > minNumber && minNumber < rightDigit) {
                minCount++;
            }
            leftDigit = minNumber;
            minNumber = rightDigit;
            rightDigit = number;
            number = scanner.nextInt();
        }
        System.out.println("Общее кол-во локальных миниумов: " + minCount);
    }
}

