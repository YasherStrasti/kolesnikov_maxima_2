import java.util.Scanner;

class Program1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int first = 0;
        int second = 0;
        int third = 0;
        int fourth = 0;
        int fifth = 0;
        int sixth = 0;
        int seventh = 0;
        int eighth = 0;

        if (number >= 0 && number <= 127) {
            first = number % 2;
            number /= 2;
            second = number % 2;
            number /= 2;
            third = number % 2;
            number /= 2;
            fourth = number % 2;
            number /= 2;
            fifth = number % 2;
            number /= 2;
            sixth = number % 2;
            number /= 2;
            seventh = number % 2;
            number /= 2;
            eighth = number % 2;
            System.out.println(eighth + "" + seventh + "" + sixth + "" + fifth + "" + fourth + "" + third + "" + second + "" + first);
        } else {
            System.out.println("Число выходит за диапазон 0 ... 127!");
        }
    }
}
